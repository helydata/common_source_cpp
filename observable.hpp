/*
 * observable.hpp
 *  learning from java.util.Observable,java.util.Observer 
 *  Created on: 2021/09/15
 *      Author: guyadong
 */

#ifndef COMMON_SOURCE_CPP_OBSERVABLE_H_
#define COMMON_SOURCE_CPP_OBSERVABLE_H_
#include <memory>
#include <mutex>
#include <vector>
#include <algorithm>
namespace observable {
	class observable;
	/**
	 * A class can implement the <code>observer</code> interface when it
	 * wants to be informed of changes in observable objects.
	 *
	 */
	struct observer : public std::enable_shared_from_this<observer> {
		/**
		 * This method is called whenever the observed object is changed. An
		 * application calls an <tt>Observable</tt> object's
		 * <code>notifyObservers</code> method to have all the object's
		 * observers notified of the change.
		 *
		 * @param   o     the observable object.
		 * @param   arg   an argument passed to the <code>notifyObservers</code>
		 *                 method.
		 */
		virtual void update(const observable &o, const std::shared_ptr<void>& arg) = 0;
	};
	using observer_ptr_type = std::shared_ptr<observer>;
	/**
	 * This class represents an observable object, or "data"
	 * in the model-view paradigm. It can be subclassed to represent an
	 * object that the application wants to have observed.
	 * An observable object can have one or more observers. An observer
	 * may be any object that implements interface <tt>observer</tt>. After an
	 * observable instance changes, an application calling the
	 * <code>Observable</code>'s <code>notifyObservers</code> method
	 * causes all of its observers to be notified of the change by a call
	 * to their <code>update</code> method.
	 * <p>
	 * The order in which notifications will be delivered is unspecified.
	 * The default implementation provided in the Observable class will
	 * notify Observers in the order in which they registered interest, but
	 * subclasses may change this order, use no guaranteed order, deliver
	 * notifications on separate threads, or may guarantee that their
	 * subclass follows this order, as they choose.
	 * <p>
	 * Note that this notification mechanism is has nothing to do with threads
	 * and is completely separate from the <tt>wait</tt> and <tt>notify</tt>
	 * mechanism of class <tt>Object</tt>.
	 * <p>
	 * When an observable object is newly created, its set of observers is
	 * empty. Two observers are considered the same if and only if the
	 * <tt>equals</tt> method returns true for them.
	 *
	 */
	class observable : public std::enable_shared_from_this<observable> {
	public:
		observable() = default;
		virtual ~observable() = default;
		observable(const observable&o) 
		{
			obs = o.obs;
			changed = o.changed;
		}
		observable& operator=(const observable&o) 
		{
			obs = o.obs;
			changed = o.changed;
			return *this;
		}
		/**
		 * Adds an observer to the set of observers for this object, provided
		 * that it is not the same as some observer already in the set.
		 * The order in which notifications will be delivered to multiple
		 * observers is not specified. See the class comment.
		 *
		 * @param   o   an observer to be added.
		 * @throws invalid_argument   if the parameter o is null.
		 */
		void addObserver(const observer_ptr_type& o) {
			std::lock_guard<std::mutex> guard(obs_mtx);
			if (!o)
			{
				throw std::invalid_argument("o is null");
			}
			auto found = std::find_if(obs.begin(), obs.end(), [&](const observer_ptr_type&e) { return o == e; });
			if (found == obs.end())
			{
				obs.emplace_back(o);
			}
		}
		/**
		 * Deletes an observer from the set of observers of this object.
		 * Passing <CODE>null</CODE> to this method will have no effect.
		 * @param   o   the observer to be deleted.
		 */
		void deleteObserver(const observer_ptr_type& o) {
			if (o) 
			{
				std::lock_guard<std::mutex> guard(obs_mtx);
				auto found = std::find_if(obs.begin(), obs.end(), [&](const observer_ptr_type&e) { return o == e; });
				obs.erase(found);
			}
		}
		/**
		 * If this object has changed, as indicated by the
		 * <code>hasChanged</code> method, then notify all of its observers
		 * and then call the <code>clearChanged</code> method to
		 * indicate that this object has no longer changed.
		 * <p>
		 * Each observer has its <code>update</code> method called with two
		 * arguments: this observable object and <code>null</code>. In other
		 * words, this method is equivalent to:
		 * <blockquote><tt>
		 * notifyObservers(null)</tt></blockquote>
		 *
		 */
		void notifyObservers() {
			notifyObservers(nullptr);
		}
		/**
		 * If this object has changed, as indicated by the
		 * <code>hasChanged</code> method, then notify all of its observers
		 * and then call the <code>clearChanged</code> method to indicate
		 * that this object has no longer changed.
		 * <p>
		 * Each observer has its <code>update</code> method called with two
		 * arguments: this observable object and the <code>arg</code> argument.
		 *
		 * @param   arg   any object.
		 */
		void notifyObservers(const std::shared_ptr<void>& arg) {
			
			/*
			* a temporary array buffer, used as a snapshot of the state of
			* current Observers.
			*/
			decltype(obs) arrLocal;
			{
				std::lock_guard<std::mutex> guard(obs_mtx);
			
				if (!changed)
				{
					return;
				}
				arrLocal = obs;
				clearChanged();
			}
			for (observer_ptr_type e : obs)
			{
				e->update(*this, arg);
			}
		}

		/**
		 * Clears the observer list so that this object no longer has any observers.
		 */
		void deleteObservers() {
			std::lock_guard<std::mutex> guard(obs_mtx);
			obs.clear();
		}

		/**
		 * Tests if this object has changed.
		 *
		 * @return  <code>true</code> if and only if the <code>setChanged</code>
		 *          method has been called more recently than the
		 *          <code>clearChanged</code> method on this object;
		 *          <code>false</code> otherwise.
		 */
		bool hasChanged() const{
			return changed;
		}
		/**
		 * Returns the number of observers of this <tt>Observable</tt> object.
		 *
		 * @return  the number of observers of this object.
		 */
		int countObservers() const{
			return (int)obs.size();
		}
	protected:
		/**
		 * Marks this <tt>Observable</tt> object as having been changed; the
		 * <tt>hasChanged</tt> method will now return <tt>true</tt>.
		 */
		void setChanged() {
			changed = true;
		}

		/**
		 * Indicates that this object has no longer changed, or that it has
		 * already notified all of its observers of its most recent change,
		 * so that the <tt>hasChanged</tt> method will now return <tt>false</tt>.
		 * This method is called automatically by the
		 * <code>notifyObservers</code> methods.
		 *
		 */
		void clearChanged() {
			changed = false;
		}
	private:
		std::vector<observer_ptr_type> obs;
		std::mutex obs_mtx;
		bool changed;
	};


} /* namespace observable */

#endif /* COMMON_SOURCE_CPP_OBSERVABLE_H_ */
