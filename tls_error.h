/*
 * tls_error.h
 *	使用TLS(thread local storage)变量保存系统运行时产生的错误信息供线程后续读取
 *  Created on: Aug 22, 2020
 *      Author: gyd
 */

#ifndef COMMON_SOURCE_CPP_TLS_ERROR_H_
#define COMMON_SOURCE_CPP_TLS_ERROR_H_
#include <sample_log.h>
namespace gdface {
// 设置错误信息
void tls_set_errmsg(const char* file,int line,const char*msg);

// 读取当前线程错误信息,如果应用程序错误信息为空，系统错误代码errnor不为0,则返回系统错误消息strerr
// 只保证读取一次，读取后错误信息被自动清除，再次调用返回空字符串
const char *tls_strerr ();

// 调用tls_strerr读取当前线程错误信息,如果为空，则返回默认信息default_msg
const char * tls_strerr_or_default(const char * default_msg);

// 输出错误信息到stderr
void tls_perr();
} /** namespace gdface */
#define TLS_SET_ERR(msg) gdface::tls_set_errmsg(__FILE__,__LINE__,msg)
#define TLS_SET_ERR_FMT(fmt,...) tls_set_errmsg(__FILE__, __LINE__, gdface::log::sample_format(fmt, ##__VA_ARGS__).c_str())

#endif /* COMMON_SOURCE_CPP_TLS_ERROR_H_ */
