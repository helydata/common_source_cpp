/*
 * uri_utils.hpp
 *
 *  Created on: 2020年4月7日
 *      Author: guyadong
 */

#ifndef COMMON_SOURCE_CPP_URI_UTILS_HPP_
#define COMMON_SOURCE_CPP_URI_UTILS_HPP_
#include <sstream>
#include <limits>
#include <uri/uri.hh>
 /* 为uri提供std::hash<uri>特例化实现 */
namespace std
{
	template<>
	struct hash<uri> {
		typedef uri argument_type;
		typedef std::size_t result_type;
		result_type operator()(argument_type const& uri) const noexcept {
			return std::hash<std::string>()(uri.to_string());
		}
	};
} /* namespace std */
// uri < 操作符
inline bool operator<(const uri&u1, const uri&u2) {
	return u1.to_string().compare(u2.to_string()) < 0;
}
#endif /* COMMON_SOURCE_CPP_URI_UTILS_HPP_ */
