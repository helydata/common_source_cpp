#ifndef COMMON_SOURCE_CPP_IF_UTILITS_H
#define COMMON_SOURCE_CPP_IF_UTILITS_H
#include <string>
/************************************************************************/
/* 网卡工具                                                             */
/************************************************************************/
namespace ifu{
//************************************
// 返回设备网卡IP地址字符串(.分割十进制)
// @param    const std::string & ifname 指定网卡名称,为空则返回第一个找到网卡设备的数据
// @param    std::string & ip_address [out] ip地址输出缓冲区
// @param    bool debugLog 是否输出调试信息
// @return   bool 调用成返回true
//************************************
bool get_ip_address(const std::string& ifname, std::string &ip_address, bool debugLog = false);
//************************************
// 返回设备网卡MAC地址字符串(12字节HEX格式)
// @param    const std::string & ifname 指定网卡名称,为空则返回第一个找到网卡设备的数据
// @param    std::string & mac_address [out] 网卡MAC地址输出缓冲区
// @param    bool debugLog 是否输出调试信息
// @return   bool 调用成返回true
//************************************
bool get_mac_address(const std::string& ifname, std::string &mac_address, bool debugLog = false);
//************************************
// 根据MAC地址(HEX字符串)查找对应的网卡
// @param    const std::string & input_mac MAC地址(HEX字符串)
// @param    std::string & ifname [out] 如果找到的匹配MAC地址网卡设备,则返回网卡设备名称,such as 'wlan0','eth0',没找到则返回空字符串
// @param    std::string & foundnics [out] 以 '$ifname0/$mac0,$ifname1/$mac1'格式返回找到的所有网卡设备,没有则返回空字符串
// @param    bool debugLog 是否输出调试信息
// @return   bool 找到匹配MAC地址的网卡返回true,否则返回false
//************************************
bool find_nic_by_mac(const std::string &input_mac, std::string& ifname, std::string &foundnics, bool debugLog = false);
//************************************
// 检查输入的mac是否与本机的设备MAC地址匹配,以参见 find_nic_by_mac
// @param    const std::string & mac HEX格式的MAC地址,such as '0A-00-27-00-00-16' or 'D0:17:C2:D0:3F:AA' or '0a0027000018'
// @param    bool debugFlag 是否输出调试信息
// @param    std::string & errmsg [out] 输出错误信息
// @return   bool 如果本机找到匹配指定MAC地址的网卡设备返回true
//************************************
bool check_mac_matched(const std::string &mac, bool debugFlag, std::string &errmsg);
} /** namespace ifu */
#endif /** COMMON_SOURCE_CPP_IF_UTILITS_H */
