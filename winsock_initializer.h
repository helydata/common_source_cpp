/*
 * winsock_initializer.h
 *
 *  Created on: 2020年4月6日
 *      Author: guyadong
 */

#ifndef COMMON_SOURCE_CPP_WINSOCK_INITIALIZER_H_
#define COMMON_SOURCE_CPP_WINSOCK_INITIALIZER_H_
#ifdef _WIN32
#include <Winsock2.h>
#include <stdexcept>

class winsock_initializer {
public:
  winsock_initializer() {
    //! Windows network DLL init
    WORD version = MAKEWORD(2, 2);
    WSADATA data;

    if (WSAStartup(version, &data) != 0) {
      throw std::runtime_error("WSAStartup() failure");
    }
  }
  ~winsock_initializer() {
	  WSACleanup();
  }
};
#else
class winsock_initializer {};
#endif /* _WIN32 */
#endif /* COMMON_SOURCE_CPP_WINSOCK_INITIALIZER_H_ */
