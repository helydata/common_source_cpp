# C++ MD5 implementation
代码来源于:
[http://www.zedwood.com/article/cpp-md5-function](http://www.zedwood.com/article/cpp-md5-function)

在该份代码基础上，修改了一些不合理的设计。代码已经在VS2015,CLang下编译通过，并在Windows AMD64,android/arm平台上测试验证通过。符合[RFC1321](https://tools.ietf.org/html/rfc1321)规范，计算结果与Java平台的MD5方法一致。

# 调用示例

	#include <iostream>
	#include <string>
	#include "md5.h"
	 
	int main(int argc, char *argv[])
	{
		std::cout << "md5 of 'grape': " << md5::digestString("grape") << std::endl;
		return 0;
	}